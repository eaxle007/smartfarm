import datetime
import celery
import shutil
from quickstart import models
import os, csv
from apscheduler.schedulers.background import BackgroundScheduler


# import absolute_import, unicode_literals


def myTask():
    vFiles = os.listdir(r"C:\Users\summit\Downloads\IoTFarmAppData")
    if len(vFiles) > 0:
        for fileName in vFiles:
            vName = r"C:\Users\summit\Downloads\IoTFarmAppData"
            vName = (os.path.join(vName, fileName))
            tank = []
            dcu = []
            sensor = []
            with open(vName, "r") as f:
                # print(vName)
                reader = csv.reader(f)
                next(reader, None)
                for row in reader:
                    dcu.append({"dcu_id": row[0]})
                    tank.append({"tank_id": row[1], "dcu_id": row[0]})
                    sensor.append(
                        {"tank_id": row[1], "dcu_id": row[0], "sensor_node_id": row[2], "date_and_time": row[3],
                         "water_temperature": row[4], "ph_value": row[5], "salinity": row[6],
                         "dissolved_oxygen": row[7]})
            # print(len(sensor))
            sensor = [dict(t) for t in {tuple(d.items()) for d in sensor}]
            tank = [dict(t) for t in {tuple(d.items()) for d in tank}]
            dcu = [dict(t) for t in {tuple(d.items()) for d in dcu}]
            dcu_batch = [models.DCU(dcu_id=row['dcu_id']) for row in dcu]
            tank_batch = [models.Tank(tank_id=row['tank_id'], dcu=models.DCU(dcu_id=row['dcu_id'])) for row in tank]
            sensor_batch = [
                models.SensorNode(tank=models.Tank(tank_id=row['tank_id']), dcu=models.DCU(dcu_id=row['dcu_id']),
                                  sensor_node_id=row[
                                      'sensor_node_id'],
                                  date_and_time=row[
                                      'date_and_time'],
                                  water_temperature=row[
                                      'water_temperature'],
                                  ph_value=row['ph_value'],
                                  salinity=row[
                                      'salinity'],
                                  dissolved_oxygen=row['dissolved_oxygen']) for row
                in sensor]
            models.DCU.objects.bulk_create(dcu_batch, ignore_conflicts=True)
            models.Tank.objects.bulk_create(tank_batch, ignore_conflicts=True)
            models.SensorNode.objects.bulk_create(sensor_batch, ignore_conflicts=True)
            dName = r"C:\Users\summit\Downloads\backup"
            t2 = (os.path.join(dName, fileName))
            print (vName)
            print(t2)
            shutil.move(vName, t2)
    print("lets go")


def start():
    scheduler = BackgroundScheduler()
    scheduler.add_job(myTask, trigger='cron', hour='15', minute='46')
    scheduler.start()
