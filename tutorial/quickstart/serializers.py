from rest_framework import serializers
from quickstart import models


class DCUSerializer(serializers.ModelSerializer):
    def create(self, validated_data):
        print("here madasdlsdldsm")
        return models.DCU.objects.create(**validated_data)

    class Meta:
        model = models.DCU
        fields = '__all__'


class ListNodeSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.SensorNode
        fields = ['sensor_node_id']


class TankSerializer(serializers.ModelSerializer):

    def create(self, validated_data):
        return models.Tank.objects.create(**validated_data)

    class Meta:
        model = models.Tank
        fields = '__all__'


class SensorNodeMinMaxSerializer(serializers.ModelSerializer):
    # tank = TankSerializer(many=True)

    class Meta:
        model = models.SensorNodeMinMax
        fields = '__all__'


class DynamicFieldsModelSerializer(serializers.ModelSerializer):
    def __init__(self, *args, **kwargs):
        super(DynamicFieldsModelSerializer, self).__init__(*args, **kwargs)
        fields = self.context['request'].query_params.get('fields', None)
        if fields is not None:
            allowed = fields.split(",")
            allowed = set(allowed)
            existing = set(self.fields)
            for field_name in existing - allowed:
                self.fields.pop(field_name)


class SensorNodeSerializer(DynamicFieldsModelSerializer):
    def create(self, validated_data):
        return models.SensorNode.objects.create(**validated_data)

    class Meta:
        model = models.SensorNode
        fields = ['sensor_node_id', 'water_temperature', 'ph_value', 'salinity', 'dissolved_oxygen', 'date_and_time',
                  'tank']
