# Create your models here.
from django.db import models


class DCUManager(models.Manager):
    def create_dcu(self, id):
        dcu = self.create(dcu_id=id)
        # do something with the book
        return dcu


class DCU(models.Model):
    dcu_id = models.CharField(max_length=30, primary_key=True)

    @classmethod
    def creaste(cls, id):
        dcu = cls(dcu_id=id)
        return dcu

    objects = DCUManager()

    class Meta:
        db_table = 'dcu'


class Tank(models.Model):
    tank_id = models.CharField(max_length=30, primary_key=True)
    dcu = models.ForeignKey(DCU, on_delete=models.CASCADE)

    class Meta:
        db_table = 'tank'


class SensorNode(models.Model):
    sensor_node_id = models.CharField(max_length=30, primary_key=True)
    tank = models.ForeignKey(Tank, on_delete=models.CASCADE, default=None)
    dcu = models.ForeignKey(DCU, on_delete=models.CASCADE, default=None)
    water_temperature = models.DecimalField(max_digits=10, decimal_places=2)
    ph_value = models.DecimalField(max_digits=10, decimal_places=2)
    salinity = models.DecimalField(max_digits=10, decimal_places=2)
    dissolved_oxygen = models.DecimalField(max_digits=10, decimal_places=2)
    date_and_time = models.CharField(max_length=30)

    class Meta:
        db_table = 'sensor_node'


class SensorNodeMinMax(models.Model):
    sensor_node_id = models.CharField(max_length=30, primary_key=True)
    tank = models.ForeignKey(Tank, on_delete=models.CASCADE, default=None)
    dcu = models.ForeignKey(DCU, on_delete=models.CASCADE, default=None)
    water_temperature_min = models.DecimalField(max_digits=10, decimal_places=2)
    water_temperature_max = models.DecimalField(max_digits=10, decimal_places=2)
    ph_value_min = models.DecimalField(max_digits=10, decimal_places=2)
    ph_value_max = models.DecimalField(max_digits=10, decimal_places=2)
    salinity_min = models.DecimalField(max_digits=10, decimal_places=2)
    salinity_max = models.DecimalField(max_digits=10, decimal_places=2)
    dissolved_oxygen_min = models.DecimalField(max_digits=10, decimal_places=2)
    dissolved_oxygen_max = models.DecimalField(max_digits=10, decimal_places=2)

    class Meta:
        db_table = 'sensor_min_max'
