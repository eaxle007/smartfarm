from rest_framework import viewsets, routers, generics
from quickstart.serializers import SensorNodeSerializer, DCUSerializer, TankSerializer, ListNodeSerializer, \
    SensorNodeMinMaxSerializer
from quickstart import models
from rest_framework.decorators import api_view
from django.views.decorators.csrf import csrf_exempt
from rest_framework.response import Response
from rest_framework import status
from django.db import connection


class DCUViewSet(viewsets.ModelViewSet):
    queryset = models.DCU.objects.all()
    serializer_class = DCUSerializer


class ConstraintViewSet(generics.ListAPIView):
    serializer_class = SensorNodeMinMaxSerializer

    def get_queryset(self):
        node = self.request.query_params.get('node')
        queryset = models.SensorNodeMinMax.objects.all()
        tank = self.request.query_params.get('tank', None)
        dcu = self.request.query_params.get('dcu', None)
        if tank is not None:
            if node is not None:
                if dcu is not None:
                    queryset = queryset.filter(tank=tank, sensor_node_id=node, dcu=dcu)
        print(queryset.query)
        return queryset


@api_view(['PUT'])
@csrf_exempt
def snippet_detail(request, pk):
    if request.method == 'PUT':
        with connection.cursor() as cursor:
            cursor.execute("update sensor_min_max set water_temperature_min = %s,"
                           "water_temperature_max = %s,ph_value_min =%s,ph_value_max = %s,"
                           "salinity_min = %s,salinity_max = %s,dissolved_oxygen_min = %s,dissolved_oxygen_max = %s"
                           " where dcu_id=%s and tank_id=%s and sensor_node_id=%s", [
                               request.data['water_temperature_min'],
                               request.data['water_temperature_max'],
                               request.data['ph_value_min'],
                               request.data['ph_value_max'],
                               request.data['salinity_min'],
                               request.data['salinity_max'],
                               request.data['dissolved_oxygen_min'],
                               request.data['dissolved_oxygen_max'],
                               request.data['dcu'],
                               request.data['tank'],
                               request.data['sensor_node_id']
                           ])
            row = cursor.fetchone()
        print(row)
        return Response(row, status=status.HTTP_200_OK)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class TankViewSet(viewsets.ModelViewSet):
    queryset = models.Tank.objects.all()
    serializer_class = TankSerializer


class SensorNodeViewSet(viewsets.ModelViewSet):
    queryset = models.SensorNode.objects.all()
    serializer_class = SensorNodeSerializer


class SensorViewSet(generics.ListAPIView):
    serializer_class = SensorNodeSerializer

    def get_queryset(self):
        node = self.request.query_params.get('node')
        queryset = models.SensorNode.objects.all()
        tank = self.request.query_params.get('tank', None)
        date = self.request.query_params.get('date', None)
        if tank is not None:
            if node is not None:
                queryset = queryset.filter(tank__tank_id=tank, sensor_node_id=node, date_and_time__contains=date)
        print(queryset.query)
        return queryset


class TankListViewSet(generics.ListAPIView):
    serializer_class = TankSerializer

    def get_queryset(self):
        queryset = models.Tank.objects.all()
        dcu = self.request.query_params.get('dcu', None)
        if dcu is not None:
            queryset = queryset.filter(dcu_id=dcu)
        return queryset


class ListNodesViewSet(generics.ListAPIView):
    serializer_class = ListNodeSerializer

    def get_queryset(self):
        # models.Shop.objects.order_by().values_list('city').distinct()
        queryset = models.SensorNode.objects.order_by().values('sensor_node_id').distinct()
        tank = self.request.query_params.get('tank', None)
        if tank is not None:
            queryset = queryset.filter(tank__tank_id=tank)
        print(queryset.query)
        return queryset


class AvgNodesViewSet(generics.ListAPIView):
    serializer_class = SensorNodeSerializer

    def get_queryset(self):
        node = self.request.query_params.get('node')
        vDate = self.request.query_params.get('date', None)
        queryset = models.SensorNode.objects.raw('''
            SELECT sensor_node_id,dcu_id,tank_id,AVG(ph_value) as ph_value,avg(salinity) as salinity, AVG(water_temperature) as water_temperature,AVG(dissolved_oxygen) as dissolved_oxygen, (date_and_time) as date_and_time FROM sensor_node WHERE sensor_node_id=%s and date(date_and_time) LIKE %s GROUP by date(date_and_time)''',
                                                 [node, '%' + vDate + '%'])
        print(queryset.query)
        return queryset
